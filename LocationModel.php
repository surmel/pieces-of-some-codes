<?php
    class LocationModel extends CI_Model{
        
        public function getAll() {
            return $this->db->select('l.*, c.ru_title as ru_city_title, c.en_title as en_city_title, d.ru_title as ru_point_title, d.en_title as en_point_title')
                            ->join('city c', 'c.id = l.city_id')
                            ->join('distribution d', 'd.id = l.point_id')                            
                            ->order_by('l.id', 'desc')
                            ->get('locations l')
                            ->result_array();
        }
        public function addLocation($location_data){            
            return $this->db->insert('locations',$location_data);             
        }
        public function getById($id){
            return $this->db->select('l.*, c.ru_title as ru_city_title, c.en_title as en_city_title, d.ru_title as ru_point_title, d.en_title as en_point_title')
                            ->join('city c', 'c.id = l.city_id')
                            ->join('distribution d', 'd.id = l.point_id')  
                            ->where('l.id = '.$id)
                            ->order_by('l.id', 'desc')
                            ->get('locations l')
                            ->row_array();
        }
        public function getLocationByCity($city_id) {
            return $this->db->select('l.coordinates, d.ru_title, d.en_title')
                            ->join('distribution d', 'd.id = l.point_id')
                            ->where('l.city_id', $city_id)
                            ->get('locations l')
                            ->result_array();
        }
        
        public function getLocationByCityAndPoint($city_id, $point_id) {
            return $this->db->select('l.coordinates, d.ru_title, d.en_title')
                            ->join('distribution d', 'd.id = l.point_id')
                            ->where(['l.city_id'=> $city_id, 'point_id' => $point_id])
                            ->get('locations l')
                            ->result_array();
        }
        public function deleteLocation($id){
            return $this->db->where('id', $id)->delete('locations');
        }

        public function editLocation($id,$data){            
            return $this->db->where('id', $id)->update('locations', $data);
        }
        
    }

