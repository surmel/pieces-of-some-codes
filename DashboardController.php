<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardController extends CI_Controller {
        function __construct() {
            parent::__construct();            
            $this->load->helper('form');
            $this->load->helper('url');
            $this->load->helper('path');
            $this->load->helper('security');
            $this->load->library(array('session', 'form_validation', 'email'));
            $this->load->model('MagazineModel');
            $this->load->model('MediakitModel');
            $this->load->model('EventsModel');
            if(!$this->session->userdata('username')){
                header('Location: /admin');
            }            
            $this->load->view('admin/header');
        }

        public function index(){
            if($this->input->post('year')){  
                if($_FILES['file']['name'] == '' || $_FILES['cover']['name'] == '' || $this->input->post('num') == '' || $this->input->post('months') == '' || $this->input->post('year') == ''){                    
                    $this->session->set_flashdata('magazine-error','<div class="alert alert-danger text-center">Заполните все поля</div>');
                    header('Location: '.$_SERVER['REQUEST_URI']);
                    exit;
                }                
                $en_months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                $ru_months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
                $months = explode('-', $this->input->post('months'));
                $num = $this->input->post('num');
                $ru_month = $ru_months[$months[0]].' - '.$ru_months[$months[1]];
                $en_month = $en_months[$months[0]].' - '.$en_months[$months[1]];
                $year = $this->input->post('year');
                
                $magazine_data = [
                    'number' => $num,
                    'ru_month' => $ru_month,                        
                    'en_month' => $en_month,                        
                    'year' => $year,

                ];
                $res = $this->MagazineModel->addMagazine($magazine_data);
                
                
                if($res){
                    $path = 'ci/userfiles/magazine/'.$res;
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    $config['upload_path'] = $path;
                    $config['encrypt_name'] = TRUE;
                    $config['allowed_types'] = 'jpg|png|jpeg|gif';                
                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('cover'))
                    {
                        $error = array('error' => $this->upload->display_errors());                                                
                    }
                    else {
                        $data = array('upload_data' => $this->upload->data());                            
                        $image = $data['upload_data']['file_name'];
                        $update_data = ['image' => $image];
                        $result = $this->MagazineModel->editMagazine($res, $update_data);                        
                    }
                    
                    $config['upload_path'] = $path;
                    $config['encrypt_name'] = TRUE;
                    $config['allowed_types'] = 'pdf';     
                    $config['max_size'] = 0;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ( ! $this->upload->do_upload('file'))
                    {
                        $error = array('error' => $this->upload->display_errors());                          
                    }
                    else {
                        $data = array('upload_data' => $this->upload->data());                            
                        $file = $data['upload_data']['file_name'];
                        $update_data = ['file_name' => $file];
                        $result = $this->MagazineModel->editMagazine($res, $update_data);                        
                    }
                    $this->session->set_flashdata('magazine-added','<div class="alert alert-success text-center">Журнал добавлен</div>');
                    header('Location: '.$_SERVER['REQUEST_URI']);
                    

                }                                
            }
            if($this->input->post('num')){
                $num = $this->input->post('num');
                $check = $this->MagazineModel->checkNumber($num);
                if($check == 0){
                    echo 'ok';
                }
                else {
                    echo 'error';
                    exit;
                }                
            }
            $data['last_date']=$this->MagazineModel->getLastDate();
            $data['magazines']=$this->MagazineModel->getAll();
            $data['events']=$this->EventsModel->getAll();
            
            $this->load->view('admin/index', $data);
            $this->load->view('admin/footer');
        }
        
        public function editMagazine($id) {
            if($this->input->post('old_file_name')){
                var_dump($_FILES);
                $en_months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                $ru_months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
                $months = explode('-', $this->input->post('months'));
                
                $ru_month = $ru_months[$months[0]].' - '.$ru_months[$months[1]];
                $en_month = $en_months[$months[0]].' - '.$en_months[$months[1]];
                $year = $this->input->post('year');
                $num = $this->input->post('num');                               
                
                if(isset($_FILES['file']['name'])){
                    $config['upload_path'] = 'ci/userfiles/magazine/'.$id;
                    $config['encrypt_name'] = TRUE;
                    $config['allowed_types'] = 'pdf|jpg';                

                    $this->load->library('upload', $config);


                    if ( ! $this->upload->do_upload('file'))
                    {
                        $error = array('error' => $this->upload->display_errors());                    
                        $file = $this->input->post('old_file_name');
                    }
                    else {                        
                        $data = array('upload_data' => $this->upload->data());
                        $path = FCPATH.'ci/userfiles/magazine/'.$id;                                                
                        if (is_dir($path)) {
                            unlink($path.'/'.$this->input->post('old_file_name'));                                                                       
                        }                 
                        $file = $data['upload_data']['file_name'];
                    }
                    
                }
                else {
                    $file = $this->input->post('old_file_name');
                }
                if(isset($_FILES['cover']['name'])){
                    $config['upload_path'] = 'ci/userfiles/magazine/'.$id;
                    $config['encrypt_name'] = TRUE;
                    $config['allowed_types'] = 'jpg|png|jpeg|gif';                

                    $this->load->library('upload', $config);


                    if ( ! $this->upload->do_upload('cover'))
                    {
                        $error = array('error' => $this->upload->display_errors());                    
                        $image = $this->input->post('old_image_name');
                    }
                    else {                        
                        $data = array('upload_data' => $this->upload->data());
                        $path = FCPATH.'ci/userfiles/magazine/'.$id;                                                
                        if (is_dir($path)) {
                            unlink($path.'/'.$this->input->post('old_image_name'));                                                                       
                        }              
                        $image = $data['upload_data']['file_name'];                        
                    }
                    
                }
                else {
                    $image = $this->input->post('old_file_name');
                }
                $magazine_data = [
                    'number' => $num,
                    'ru_month' => $ru_month,
                    'en_month' => $en_month,
                    'year' => $year,
                    'image' => $image,
                    'file_name' => $file 
                ];
                $result = $this->MagazineModel->editMagazine($id, $magazine_data); 
                if($result){
                    $this->session->set_flashdata('magazine-added','<div class="alert alert-success text-center">Данные обновлены</div>');
                    header('Location: /admin/dashboard');
                }
            }
            
            
            $data['magazine'] = $this->MagazineModel->getById($id);
            $this->load->view('admin/editMagazine',$data);
            $this->load->view('admin/footer');
        }
        
        public function deleteMagazine(){
            if($this->input->post('delete_magazine')){
                $id = $this->input->post('id');
                $file_name = $this->MagazineModel->getFileName($id);                                
                $path = FCPATH.'ci/userfiles/magazine/'.$id; 
                if($file_name['file_name'] && $file_name['image']){                    
                    if (is_dir($path)) {
                        unlink($path.'/'.$file_name['file_name']);
                        unlink($path.'/'.$file_name['image']);
                        rmdir($path) or die('failed deleting: ' . $path);                    
                    } 
                }

                $res = $this->MagazineModel->deleteMagazine($id);
                if($res){
                    echo "done";
                    exit;
                }
                else {
                    echo "error";
                    exit;
                }                
            }
        }
        public function deleteMediakit(){
            if($this->input->post('delete_mediakit')){
                $id = $this->input->post('id');
                $file_name = $this->MediakitModel->getFileName($id);                                
                $path = FCPATH.'ci/userfiles/mediakit/'.$id; 
                if($file_name['file_name'] && $file_name['image']){                    
                    if (is_dir($path)) {
                        unlink($path.'/'.$file_name['file_name']);
                        unlink($path.'/'.$file_name['image']);
                        rmdir($path) or die('failed deleting: ' . $path);                    
                    } 
                }

                $res = $this->MediakitModel->deleteMediakit($id);
                if($res){
                    echo "done";
                    exit;
                }
                else {
                    echo "error";
                    exit;
                }                
            }
        }
        public function deleteEvent(){
            if($this->input->post('delete_event')){
                $id = $this->input->post('id');
                $file_name = $this->EventsModel->getFileName($id);
                if($file_name){
                    $path = FCPATH.'ci/userfiles/events/'.$id;                
                    if (is_dir($path)) {
                        unlink($path.'/'.$file_name['image']);
                        rmdir($path) or die('failed deleting: ' . $path);                    
                    }
                }
                
                $res = $this->EventsModel->deleteEvent($id);
                if($res){
                    echo "done";
                    exit;
                }
                else {
                    echo "error";
                    exit;
                }                
            }
        }
        
        public function addEvent() {
            if($this->input->post('addEvent')){                
                $ru_title = $this->input->post('ru_title');
                $en_title = $this->input->post('en_title');
                $ru_text = $this->input->post('ru_text');
                $en_text = $this->input->post('en_text');
                $date = $this->input->post('date');
                $event_data = [
                    'ru_title' => $ru_title,
                    'en_title' => $en_title,
                    'ru_text' => $ru_text,
                    'en_text' => $en_text,
                    'date_time' => $date
                ];
                $result = $this->EventsModel->addEvent($event_data);      
//                var_dump($result);exit;
                if($result){
                    if($_FILES['file']['name'] != ''){
                        $path = 'ci/userfiles/events/'.$result;
                        if (!file_exists($path)) {
                            mkdir($path, 0777, true);
                        }
                        $config['upload_path'] = $path;
                        $config['encrypt_name'] = TRUE;
                        $config['allowed_types'] = 'jpg|png|jpeg|gif';                
                        $this->load->library('upload', $config);


                        if ( ! $this->upload->do_upload('file'))
                        {
                            $error = array('error' => $this->upload->display_errors());                                                
                        }
                        else {
                            $data = array('upload_data' => $this->upload->data());                            
                            $image = $data['upload_data']['file_name'];
                            $update_data = ['image' => $image];
                            $res = $this->EventsModel->editEvent($result, $update_data);
                            if($res){
                                $this->session->set_flashdata('magazine-added','<div class="alert alert-success text-center">Событие добавлено</div>');
                                header('Location: /admin/dashboard');
                            }
                        }
                    }
                    else {
                        $this->session->set_flashdata('magazine-added','<div class="alert alert-success text-center">Событие добавлено</div>');
                        header('Location: /admin/dashboard');
                    }                    
                }                
            }
        }
        
        public function editEvent($id){
            if($this->input->post('editEvent')){                  
                $ru_title = $this->input->post('ru_title');
                $en_title = $this->input->post('en_title');
                $ru_text = $this->input->post('ru_text');
                $en_text = $this->input->post('en_text');
                $date = $this->input->post('date');
                                
                if(isset($_FILES['cover']['name'])){
                    $config['upload_path'] = 'ci/userfiles/events/'.$id;
                    $config['encrypt_name'] = TRUE;
                    $config['allowed_types'] = 'jpg|png|jpeg|gif';             

                    $this->load->library('upload', $config);


                    if ( ! $this->upload->do_upload('cover'))
                    {
                        $error = array('error' => $this->upload->display_errors());                    
                        $image = $this->input->post('old_image_name');
                    }
                    else {                        
                        $data = array('upload_data' => $this->upload->data());
                        $path = FCPATH.'ci/userfiles/events/'.$id;                                                
                        if (is_dir($path)) {
                            unlink($path.'/'.$this->input->post('old_image_name'));                                                                       
                        }                 
                        $image = $data['upload_data']['file_name'];
                    }
                    
                }
                else {
                    $image = $this->input->post('old_image_name');
                }                
                
                $event_data = [
                    'ru_title' => $ru_title,
                    'en_title' => $en_title,
                    'ru_text' => $ru_text,
                    'en_text' => $en_text,
                    'image' => $image,
                    'date_time' => $date
                ];
                $result = $this->EventsModel->editEvent($id, $event_data);
                if($result){
                    $this->session->set_flashdata('magazine-added','<div class="alert alert-success text-center">Данные обновлены</div>');
                    header('Location: /admin/dashboard');
                }
            }
            
            $data['event'] = $this->EventsModel->getById($id);
            $this->load->view('admin/editEvent',$data);
            $this->load->view('admin/footer');            
        }


//        public function addCategory(){
////            $data['menu']=$this->CategoriesModel->getAllMenu();
//            if($this->input->post('addNow')){
//
//                $category_name = $this->input->post('category_name');
//                $parent = $this->input->post('parent');
//
//                if($category_name == ''){
//                    $this->session->set_flashdata('error','<div class="alert alert-danger text-center">Enter Category Name</div>');
//                    header('Location: '.$_SERVER['REQUEST_URI']);
//                    exit;
//                }
//                $checkCategories = $this->CategoriesModel->checkCategory($category_name);
//                if($checkCategories > 0){
//                    $this->session->set_flashdata('error','<div class="alert alert-danger text-center">Category already exists</div>');
//                    header('Location: '.$_SERVER['REQUEST_URI']);
//                    exit;
//                }
//                else{
//                    $addMenu = [
//                        'category_name' => $category_name,
//                        'parent' => $parent
//
//                    ];
//                    $category_id = $this->CategoriesModel->addMenu($addMenu);
//                    $product_name = $this->input->post('product_name');
//                    $product_price = $this->input->post('product_price');
//                    $product_count = $this->input->post('product_count');
//                    $description = $this->input->post('description');
//
//                    $product_data = [
//                        'product_name' => $product_name,
//                        'description' =>  $description,
//                        'price' => $product_price,
//                        'count' => $product_count,
//                        'catalog_id' => $category_id,
//                    ];
//
//                    $prod_id = $this->ProductsModel->addProduct($product_data);
//
//                    $image_name = $_FILES['uploadedimages']['name'];
//                    if($image_name[0] == ''){
//                        $this->session->set_flashdata('error','<div class="alert alert-danger text-center">Choose Image</div>');
//                        header('Location: '.$_SERVER['REQUEST_URI']);
//                    }
//
//                    if($_FILES['uploadedimages']['name'] != ''){
//                        $number_of_files = sizeof($_FILES['uploadedimages']['tmp_name']);
//
//                        $files = $_FILES['uploadedimages'];
//                        if (!is_dir(base_url("public/images/products/".$prod_id))) {
//                            mkdir('./public/images/products/'.$prod_id, 0777, TRUE);
//                        }
//                        else {
//                            if (!is_dir(base_url('public/images/products/').$prod_id)) {
//                                mkdir('./public/images/products/'.$prod_id, 0777, TRUE);
//                            }
//                        }
//                        $errors = array();
//                        for($i=0;$i<$number_of_files;$i++){
//                            if($_FILES['uploadedimages']['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES['uploadedimages']['name'][$i];
//                        }
//                        if(sizeof($errors)==0){
//                            $this->load->library('upload');
//
//                            $config['upload_path'] = FCPATH.'public/images/products/'.$prod_id;
//
//                            $config['allowed_types'] = 'gif|jpg|jpeg|png';
//                            $config['overwrite'] = FALSE;
//                            for ($i = 0; $i < $number_of_files; $i++) {
//                                $date = date_create();
//                                $date = date_format($date,"F d, Y");
//                                $time = date_create();
//                                $time = date_format($time, "H:i A");
//                                $_FILES['uploadedimage']['name'] = $files['name'][$i];
//                                $explode = explode('.', $files['name'][$i]);
//                                $ext = end($explode);
//                                $files['name'][$i] = md5($i.$date.$time).'.'.$ext;
//                                $config['file_name'] = $files['name'][$i];
//                                $_FILES['uploadedimage']['type'] = $files['type'][$i];
//                                $_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
//                                $_FILES['uploadedimage']['error'] = $files['error'][$i];
//                                $_FILES['uploadedimage']['size'] = $files['size'][$i];
//
//                                $this->upload->initialize($config);
//
//                                if ($this->upload->do_upload('uploadedimage')){
//                                    $data['uploads'][$i] = $this->upload->data();
//                                    if($i == 0){
//                                        $prod_imgs_data = array (
//                                            'prod_id' => $prod_id,
//                                            'img_name' => $files['name'][$i],
//                                            'primary_image' => 1,
//                                        );
//                                    }
//                                    else {
//                                        $prod_imgs_data = array (
//                                            'prod_id' => $prod_id,
//                                            'img_name' => $files['name'][$i],
//                                            'primary_image' => 0,
//                                        );
//                                    }
//                                    $this->ProductImagesModel->addImages($prod_imgs_data);
//                                }
//                                else{
//                                    $data['upload_errors'][$i] = $this->upload->display_errors();
//                                }
//                            }
//                        }
//                        else{
//                            print_r($errors);
//                        }
//                    }
//                    header('Location: '.$_SERVER['REQUEST_URI']);
//                    exit;
//                }
//
//            }
//            if($this->input->post('addCategory')){
//                $category_name = $this->input->post('category_name');
//                $parent = $this->input->post('parent');
//                if($category_name == ''){
//                    $this->session->set_flashdata('error','<div class="alert alert-danger text-center">Enter Category Name</div>');
//                    header('Location: '.$_SERVER['REQUEST_URI']);
//                    exit;
//                }
//                $checkCategories = $this->CategoriesModel->checkCategory($category_name);
//                if($checkCategories > 0){
//                    $this->session->set_flashdata('error','<div class="alert alert-danger text-center">Category already exists</div>');
//                    header('Location: '.$_SERVER['REQUEST_URI']);
//                    exit;
//                }
//                else{
//                    $addMenu = [
//                        'category_name' => $category_name,
//                        'parent' => $parent
//                    ];
//                    $this->CategoriesModel->addMenu($addMenu);
//                    header('Location: '.$_SERVER['REQUEST_URI']);
//                }
//
//            }
//
//            $this->load->view('admin/addCategory', $data);
//        }
        
//        public function editCategory($id){
//
//            $data['category'] = $this->CategoriesModel->getMenuById($id);
//            if($this->input->post('editCategory')){
//                $category_name = $this->input->post('category_name');
//                $parent = $this->input->post('parent');
//
//                if($category_name == ''){
//                    $this->session->set_flashdata('error','<div class="alert alert-danger text-center">Enter Category Name</div>');
//                    header('Location: '.$_SERVER['REQUEST_URI']);
//                    exit;
//                }
//                $checkCategories = $this->CategoriesModel->checkCategory($category_name);
//                if($checkCategories > 0){
//                    $this->session->set_flashdata('error','<div class="alert alert-danger text-center">Category already exists</div>');
//                    header('Location: '.$_SERVER['REQUEST_URI']);
//                    exit;
//                }
//                else{
//                    $editMenu = [
//                        'category_name' => $category_name,
//                        'parent' => $parent
//                    ];
//                    $this->CategoriesModel->updateCategory($id, $editMenu);
//                    header('Location: /admin/addCategory');
//                    exit;
//                }
//
//            }
//
//            $this->load->view('admin/editCategory', $data);
//        }
        
//        public function delete(){
//            if($this->input->post('categoryId')){
//                $id = $this->input->post('categoryId');
//
//                $result = $this->CategoriesModel->deleteCategory($id);
//
//                if($result){
//                    $product_ids = $this->ProductsModel->getProductsIds($id);
//                    $res = $this->ProductsModel->deleteCatalogProduct($id);
//                    if($res){
//                        foreach($product_ids as $prod_id){
//                            $path = FCPATH.('public/images/products/'.$prod_id['id']);
//                            $this->load->helper("file");
//                            delete_files($path);
//                            if(rmdir($path)){
//
//                            }
//                            else{
//                                echo "not deleted";die;
//                            }
//                            $res = $this->ProductImagesModel->deleteAllImages($prod_id['id']);
//
//                        }
//                    }
//                    echo 1;
//                    exit;
//                }
//            }
//            if($this->input->post('deleteProduct')){
//                $id = $this->input->post('id');
//                $result = $this->ProductsModel->deleteProduct($id);
//                if($result){
//                    $path = FCPATH.('public/images/products/'.$id);
//                    $this->load->helper("file");
//                    delete_files($path);
//                    if(rmdir($path)){
//
//                    }
//                    else{
//                        echo "not deleted";die;
//                    }
//                    $res = $this->ProductImagesModel->deleteAllImages($id);
//                    if($res){
//                        echo 'deleted';
//                        exit;
//                    }
//                }
//            }
//
//            if($this->input->post('deleteImage')){
//                $image_id = $this->input->post('id');
//                $status = $this->ProductImagesModel->getStatus($image_id);
//                if($status['primary_image'] == 1){
//                    $this->session->set_flashdata('error','<div class="alert alert-danger text-center">You cant delete primary photo</div>');
//                    echo 'primary';
//                    exit;
//                }
//                $path = FCPATH.('public/images/products/'.$status['prod_id'].'/'.$status['img_name']);
//                if (unlink($path)) {
//                    var_dump("success");
//                } else {
//                    var_dump("fail");
//                }
//                $res = $this->ProductImagesModel->deleteImage($image_id);
//                if($res){
//                    echo 'done';
//                    exit;
//                }
//            }
//            if($this->input->post('package_id')){
//                $pack_id = $this->input->post('package_id');
//                $res = $this->PackagesModel->deletePackage($pack_id);
//                if($res){
//                    echo 1;
//                    exit;
//                }
//            }
//        }
        
//        public function products(){
//            $data['categories'] = $this->CategoriesModel->getAllMenu();
//            $data['select'] = $this->ProductsModel->selectAllProductsPrimary();
//            $this->load->library('ckeditor');
//            $this->load->library('ckfinder');
//            $this->ckeditor->basePath = base_url().'public/ckeditor/';
//            $this->ckeditor->config['language'] = 'en';
//            $this->ckeditor->config['width'] = '100%';
//            $this->ckeditor->config['height'] = '300px';
//            //Add Ckfinder to Ckeditor
//            $this->ckfinder->SetupCKEditor($this->ckeditor,'../public/ckfinder/');
//            $this->form_validation->set_rules('description', 'Page Description', 'trim|required');
//            if ($this->form_validation->run() == FALSE) {
//                $this->load->view('admin/products', $data);
//            }
//            else if($this->input->post('addProduct')){
//
//                $product_name = $this->input->post('product_name');
//                $product_price = $this->input->post('product_price');
//                $product_count = $this->input->post('product_count');
//                $description = $this->input->post('description');
//                $category_ids = $this->input->post('product_category');
//                $category_names = [];
//                foreach($category_ids as $category_id){
//                    $cat_name = $this->CategoriesModel->getMenuById($category_id);
//                    array_push($category_names, $cat_name['category_name']);
//                }
//                $category_names = implode(', ',$category_names);
//                $category_ids = implode(', ',$category_ids);
//
//                $product_data = [
//                    'product_name' => $product_name,
//                    'description' =>  $description,
//                    'price' => $product_price,
//                    'count' => $product_count,
//                    'catalog_id' => $category_ids,
//                    'catalog_names' => $category_names
//                ];
//
//                $prod_id = $this->ProductsModel->addProduct($product_data);
//
//                $image_name = $_FILES['uploadedimages']['name'];
//                if($image_name[0] == ''){
//                    $this->session->set_flashdata('error','<div class="alert alert-danger text-center">Choose Image</div>');
//                    header('Location: '.$_SERVER['REQUEST_URI']);
//                }
//
//                if($_FILES['uploadedimages']['name'] != ''){
//                    $number_of_files = sizeof($_FILES['uploadedimages']['tmp_name']);
//
//                    $files = $_FILES['uploadedimages'];
//                    if (!is_dir(base_url("public/images/products/".$prod_id))) {
//                        mkdir('./public/images/products/'.$prod_id, 0777, TRUE);
//                    }
//                    else {
//                        if (!is_dir(base_url('public/images/products/').$prod_id)) {
//                            mkdir('./public/images/products/'.$prod_id, 0777, TRUE);
//                        }
//                    }
//                    $errors = array();
//                    for($i=0;$i<$number_of_files;$i++){
//                        if($_FILES['uploadedimages']['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES['uploadedimages']['name'][$i];
//                    }
//                    if(sizeof($errors)==0){
//                        $this->load->library('upload');
//
//                        $config['upload_path'] = FCPATH.'public/images/products/'.$prod_id;
//
//                        $config['allowed_types'] = 'gif|jpg|jpeg|png';
//                        $config['overwrite'] = FALSE;
//                        for ($i = 0; $i < $number_of_files; $i++) {
//                            $_FILES['uploadedimage']['name'] = $files['name'][$i];
//                            $date = date_create();
//                            $date = date_format($date,"F d, Y");
//                            $time = date_create();
//                            $time = date_format($time, "H:i A");
//                            $explode = explode('.', $files['name'][$i]);
//                            $ext = end($explode);
//                            $files['name'][$i] = md5($i.$date.$time).'.'.$ext;
//                            $config['file_name'] = $files['name'][$i];
//                            $_FILES['uploadedimage']['type'] = $files['type'][$i];
//                            $_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
//                            $_FILES['uploadedimage']['error'] = $files['error'][$i];
//                            $_FILES['uploadedimage']['size'] = $files['size'][$i];
//
//                            $this->upload->initialize($config);
//
//                            if ($this->upload->do_upload('uploadedimage')){
//                                $data['uploads'][$i] = $this->upload->data();
//                                if($i == 0){
//                                    $prod_imgs_data = array (
//                                        'prod_id' => $prod_id,
//                                        'img_name' => $files['name'][$i],
//                                        'primary_image' => 1,
//                                    );
//                                }
//                                else {
//                                    $prod_imgs_data = array (
//                                        'prod_id' => $prod_id,
//                                        'img_name' => $files['name'][$i],
//                                        'primary_image' => 0,
//                                    );
//                                }
//                                $this->ProductImagesModel->addImages($prod_imgs_data);
//                            }
//                            else{
//                                $data['upload_errors'][$i] = $this->upload->display_errors();
//                            }
//                        }
//                    }
//                    else{
//                        print_r($errors);
//                    }
//                }
//                header('Location: '.$_SERVER['REQUEST_URI']);
//            }
//            if($this->input->post('show_all')){
//                $cat_data = $this->ProductsModel->selectAllProductsPrimary();
//                echo json_encode($cat_data);
//                exit;
//            }
//            if($this->input->post('addPackage')){
//                if(!$this->input->post('p_id')){
//                    $this->session->set_flashdata('error','<div class="alert alert-danger text-center">Choose at least one product</div>');
//                    header('Location: '.$_SERVER['REQUEST_URI']);
//                    exit;
//                }
//
//                $pack_name = $this->input->post('package_name');
//                $pack_price = $this->input->post('package_price');
//                $pack_prods = implode(',', $this->input->post('p_id'));
//                $package_data = [
//                    'package_name' => $pack_name,
//                    'package_price' => $pack_price,
//                    'package_prods' => $pack_prods
//                ];
//                $res = $this->PackagesModel->addPackage($package_data);
//                if($res){
//                    $this->session->set_flashdata('error','<div class="alert alert-success text-center">Package successfully added.</div>');
//                    header('Location: '.$_SERVER['REQUEST_URI']);
//                    exit;
//                }
//            }
//        }
        
//        public function editProduct($id){
//            $data['categories'] = $this->CategoriesModel->getAllMenu();
//            $data['product'] = $this->ProductsModel->selectProductForEdit($id);
//            $data['images'] = $this->ProductImagesModel->selectImages($id);
//            $this->load->library('ckeditor');
//            $this->load->library('ckfinder');
//            $this->ckeditor->basePath = base_url().'public/ckeditor/';
//            $this->ckeditor->config['language'] = 'en';
//            $this->ckeditor->config['width'] = '100%';
//            $this->ckeditor->config['height'] = '300px';
//            //Add Ckfinder to Ckeditor
//            $this->ckfinder->SetupCKEditor($this->ckeditor,'../public/ckfinder/');
//            $this->form_validation->set_rules('description', 'Page Description', 'trim|required');
//            if ($this->form_validation->run() == FALSE) {
//                $this->load->view('admin/editProduct', $data);
//            }
//            else if($this->input->post('prod_id')){
//                $id = $this->input->post('prod_id');
//                $product_name = $this->input->post('product_name');
//                $product_price = $this->input->post('product_price');
//                $product_count = $this->input->post('product_count');
//                $description = $this->input->post('description');
//                $countCheck = $this->ProductsModel->selectProductForEdit($id);
//                if($countCheck['count'] == 0 && $product_count != 0){
//                    $request_emails = $this->UpriseRequestModel->requestEmails($id);
//                    if($request_emails){
//                        foreach($request_emails as $email){
//                            $config['protocol'] = 'smtp';
//                            $config['smtp_host'] = 'ssl://smtp.gmail.com';
//                            $config['smtp_port'] = '465';
//                            $config['smtp_user'] = 'imvaxoaxper@gmail.com';
//                            $config['smtp_pass'] = 'vaxoaxper';
//                            $config['mailtype'] = 'html';
//                            $config['charset'] = 'utf-8';
//                            $config['wordwrap'] = TRUE;
//                            $config['newline'] = "\r\n"; //use double quotes
//                            $this->email->initialize($config);
//
//                            $message = '<p>Hello Mr. '.$email['last_name'].'.<br> We are glad to inform you that we already have the "'.$email['product_name'].'" in stock.</p>';
//
//                            $this->email->from('test@test.am');
//                            $this->email->to($email['email']);
//                            $this->email->subject('Message from web site');
//                            $this->email->message($message);
//                            if ($this->email->send())
//                            {
//                                $this->UpriseRequestModel->deleteRequest($id, $email['email']);
////                                $this->session->set_flashdata('error','<div class="alert alert-success text-center">Password recovery link sent to Your E-Mail!</div>');
//                            }
//                            else {
//                                var_dump('something is wrong');exit;
//                            }
//                        }
//                    }
//                }
//                $category_ids = $this->input->post('product_category');
//                $category_names = [];
//                foreach($category_ids as $category_id){
//                    $cat_name = $this->CategoriesModel->getMenuById($category_id);
//                    array_push($category_names, $cat_name['category_name']);
//                }
//                $category_names = implode(', ',$category_names);
//                $category_ids = implode(', ',$category_ids);
//
//                $product_data = [
//                    'product_name' => $product_name,
//                    'description' =>  $description,
//                    'price' => $product_price,
//                    'count' => $product_count,
//                    'catalog_id' => $category_ids,
//                    'catalog_names' => $category_names
//                ];
//
//                $result = $this->ProductsModel->updateProduct($id, $product_data);
//                if($result){
//                    if($_FILES['uploadedimages']['name'][0] == ''){
//                        header('Location: '.$_SERVER['REQUEST_URI']);
//                    }
//                    else {
//                        $image_name = $_FILES['uploadedimages']['name'];
//                        if($_FILES['uploadedimages']['name'] != ''){
//                            $number_of_files = sizeof($_FILES['uploadedimages']['tmp_name']);
//
//                            $files = $_FILES['uploadedimages'];
//
//                            $errors = array();
//                            for($i=0;$i<$number_of_files;$i++){
//                                if($_FILES['uploadedimages']['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES['uploadedimages']['name'][$i];
//                            }
//                            if(sizeof($errors)==0){
//                                $this->load->library('upload');
//
//                                $config['upload_path'] = FCPATH.'public/images/products/'.$id;
//
//                                $config['allowed_types'] = 'gif|jpg|jpeg|png';
//                                $config['overwrite'] = FALSE;
//                                for ($i = 0; $i < $number_of_files; $i++) {
//                                    $date = date_create();
//                                    $date = date_format($date,"F d, Y");
//                                    $time = date_create();
//                                    $time = date_format($time, "H:i A");
//                                    $_FILES['uploadedimage']['name'] = $files['name'][$i];
//                                    $explode = explode('.', $files['name'][$i]);
//                                    $ext = end($explode);
//                                    $files['name'][$i] = md5($i.$date.$time).'.'.$ext;
//                                    $config['file_name'] = $files['name'][$i];
//                                    $_FILES['uploadedimage']['type'] = $files['type'][$i];
//                                    $_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
//                                    $_FILES['uploadedimage']['error'] = $files['error'][$i];
//                                    $_FILES['uploadedimage']['size'] = $files['size'][$i];
//
//                                    $this->upload->initialize($config);
//
//                                    if ($this->upload->do_upload('uploadedimage')){
//                                        $data['uploads'][$i] = $this->upload->data();
//                                        $prod_imgs_data = array (
//                                            'prod_id' => $id,
//                                            'img_name' => $files['name'][$i],
//                                            'primary_image' => 0,
//                                        );
//
//                                        $this->ProductImagesModel->addImages($prod_imgs_data);
//                                    }
//                                    else{
//                                        $data['upload_errors'][$i] = $this->upload->display_errors();
//                                    }
//                                }
//                            }
//                            else{
//                                print_r($errors);
//                            }
//                        }
//                        header('Location: '.$_SERVER['REQUEST_URI']);
//                    }
//                }
//            }
//        }
        
//        public function editPackage($id){
//            if($this->input->post('pack_id')){
//
//                if(!$this->input->post('p_id')){
//                    $this->session->set_flashdata('error','<div class="alert alert-danger text-center">Choose at least one product</div>');
//                    header('Location: '.$_SERVER['REQUEST_URI']);
//                    exit;
//                }
//                $pack_id = $this->input->post('pack_id');
//                $pack_name = $this->input->post('package_name');
//                $pack_price = $this->input->post('package_price');
//                $pack_prods = implode(',', $this->input->post('p_id'));
//                $package_data = [
//                    'package_name' => $pack_name,
//                    'package_price' => $pack_price,
//                    'package_prods' => $pack_prods
//                ];
//
//                $res = $this->PackagesModel->updatePackage($pack_id,$package_data);
//                if($res){
//                    $this->session->set_flashdata('error','<div class="alert alert-success text-center">Package successfully updated.</div>');
//                    header('Location: '.$_SERVER['REQUEST_URI']);
//                    exit;
//                }
//
//            }
//            $packages = $this->PackagesModel->selectPackageForEdit($id);
//            $data['prod_ids'] = explode(',',$packages['package_prods']);
//            $data['all_prods'] = $this->ProductsModel->selectAllProductsPrimary();
//            $data['packages'] = $packages;
//            $this->load->view('admin/editPackage', $data);
//        }
        public function setPrimary(){            
            $id = $this->input->post('id');
            $prod_id = $this->ProductImagesModel->getById($id); 
            $primary = array (
                'primary_image' => 0
            );
            $res = $this->ProductImagesModel->clearStatus($prod_id['prod_id'], $primary);
            if($res){
                $primary = array (
                    'primary_image' => 1
                );
                $set_status = $this->ProductImagesModel->setStatus($id, $primary);
                echo $set_status;
            }                        
        }
        
//        public function packages() {
//            $packages = $this->PackagesModel->selectPackages();
//
//            foreach($packages as $package){
//                $asa[$package['package_name']] = [];
//                array_push($asa[$package['package_name']], ['package_id' => $package['id'], 'package_price' => $package['package_price']]);
//                $prod_ids = explode(',',$package['package_prods']);
//                foreach($prod_ids as $id){
//                    $product = $this->ProductsModel->selectPackageProductsPrimary($id);
//                    foreach($product as $prod){
//                        array_push($asa[$package['package_name']],$prod);
//                    }
//                }
//            }
//            $data['packages'] = $asa;
//            $this->load->view('admin/packages', $data);
//        }
}
