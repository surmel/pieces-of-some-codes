<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Magazine</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />    
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('public/plugins/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css" />        
        <link href="<?=base_url('public/plugins/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('public/plugins/bootstrap-switch/css/bootstrap-switch.min.css')?>" rel="stylesheet" type="text/css" />        
        <link href="<?=base_url('public/css/magnific-popup.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('public/css/timetable.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('public/css/eventCalendar.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('public/css/eventCalendar_theme_responsive.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('public/css/components.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('public/css/magnific-popup.css')?>" rel="stylesheet" type="text/css" />
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />               
        <link href="<?=base_url('public/css/style.css')?>" rel="stylesheet" type="text/css" />        
        <link rel="shortcut icon" href="/public/img/gavel31.png" /> 
    </head>

    <body> 
        <div class="top_line">
            <div class="container" style='padding:0px;'>
                <div class="left">               
                    <ul class="top_right_ul">
                        <li class="top_right_li"><a class="top_li_a top_about" href="<?= base_url('about')?>"><?=$this->lang->line('about');?></a></li>
                        <li class="top_right_li"><a class="top_li_a top_distribution" href="<?= base_url('distribution')?>"><?=$this->lang->line('distribution');?></a></li>
                        <li class="top_right_li"><a class="top_li_a top_advertise" href="<?= base_url('advertising')?>"><?=$this->lang->line('advertise');?></a></li>
                        <li class="top_right_li"><a class="top_li_a top_contact" href="<?= base_url('contact')?>"><?=$this->lang->line('contact');?></a></li>
                    </ul>                
                </div>
                <div class="right">
                    <?php if($this->session->userdata('lang') === 'ru'):?>
                        <p class="lang_p"><a href="/changeLang/en"><img src="<?=base_url('public/img/eng.png')?>" width="20px"> English</a></p>
                    <?php else:?>
                        <p class="lang_p"><a href="/changeLang/ru"><img src="<?=base_url('public/img/rus.png')?>" width="20px"> Русский</a></p>
                    <?php endif;?>                                               
                </div>
            </div>
            
        </div>
        <div class="menu_bar">
            <div class="container">
                <div class='logo'>
<!--                    <p class='lodka-magazine'>Lodka</p>
                    <p class='lodka-magazine'>Magazine</p>-->
                    <a href="<?= base_url()?>"><img src="<?=base_url('public/img/lodka-logo.png')?>" class="page_logo"></a>
                </div>
                
                <ul class="main_menu">
                    <!--<li class="menu_item"><a href="<?= base_url()?>" class="menu_a menu_main active_menu"><?=$this->lang->line('main');?></a></li>-->
                    <li class="menu_item"><a href="<?= base_url('yachts')?>" class="menu_a menu_yachts"><?=$this->lang->line('yachts');?></a></li>
                    <li class="menu_item"><a href="<?= base_url('werfi')?>" class="menu_a menu_werfi"><?=$this->lang->line('werfi');?></a></li>
                    <li class="menu_item"><a href="<?= base_url('marines')?>" class="menu_a menu_marines"><?=$this->lang->line('marines');?></a></li>
                    <li class="menu_item"><a href="<?= base_url('travel')?>" class="menu_a menu_travel"><?=$this->lang->line('travel');?></a></li>
                    <li class="menu_item"><a href="<?= base_url('lifestyle')?>" class="menu_a menu_lifestyle"><?=$this->lang->line('lifestyle');?></a></li>
                </ul>
                <div class="search_div">
<!--                    <div class="form-group" style="float:left;">                        
                        <input type="text" class="form-control" style="border-radius: 5px !important;">                        
                    </div>
                    <i class="fa fa-search" aria-hidden="true"></i>-->
                <div class="form-group">
                    <div class="icon-addon addon-lg">
                        <form action="/search" method="post">
                            <input type="text" placeholder="<?=$this->lang->line('search');?>" class="form-control" name="search" id="search">
                            <label for="search" class="glyphicon glyphicon-search" rel="tooltip" title="search"></label>
                            <input type="submit" name="submit" value="" style="display: none;">
                        </form>
                    </div>
                </div>
                </div>
                <a href="#" class="menu menu_rows"><span></span></a>
                <div class="mobile_menu">
                    
                    <div class="mobile_menu_list">
                        <!--<ul class="m_menu">-->
                            <a href="<?= base_url()?>" class="mobile_menu_a"><p class="mobile_menu_item mobile_menu_main mobile_active_menu"><?=$this->lang->line('main');?></p></a>
                            <a href="<?= base_url('yachts')?>" class="mobile_menu_a"><p class="mobile_menu_item mobile_menu_yachts"><?=$this->lang->line('yachts');?></p></a>
                            <a href="<?= base_url('werfi')?>" class="mobile_menu_a"><p class="mobile_menu_item mobile_menu_werfi"><?=$this->lang->line('werfi');?></p></a>
                            <a href="<?= base_url('marines')?>" class="mobile_menu_a"><p class="mobile_menu_item mobile_menu_marines"><?=$this->lang->line('marines');?></p></a>
                            <a href="<?= base_url('travel')?>" class="mobile_menu_a"><p class="mobile_menu_item mobile_menu_travel"><?=$this->lang->line('travel');?></p></a>
                            <a href="<?= base_url('lifestyle')?>" class="mobile_menu_a"><p class="mobile_menu_item mobile_menu_lifestyle"><?=$this->lang->line('lifestyle');?></p></a>
                        <!--</ul>-->
                    </div>
                </div>
            </div>
        </div>