<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdvertisingController extends CI_Controller {
        function __construct() {
            parent::__construct();            
            $this->load->helper('form');
            $this->load->helper('url');
            $this->load->helper('path');
            $this->load->helper('security');
            $this->load->model('AdvertisingModel');
            $this->load->model('MediakitModel');
            $this->load->library(array('session', 'form_validation', 'email'));
            if(!$this->session->userdata('username')){
                header('Location: /admin');
            }
            $data['error']='';
            $this->load->helper('ckeditor');
             
            $this->data['ckeditor'] = array(
                    
                    'id' 	=> 	'content',
                    'path'	=>	'public/plugins/ckeditor',
                    
                    'config' => array(
                            'toolbar' 	=> 	"Full", 	
                            'width' 	=> 	"100%",	
                            'height' 	=> 	'100px',

                    ),

                    'styles' => array(
			
                            'style 1' => array (
                                    'name' 		=> 	'Blue Title',
                                    'element' 	=> 	'h2',
                                    'styles' => array(
                                            'color' 	=> 	'Blue',
                                            'font-weight' 	=> 	'bold'
                                    )
                            ),

                            'style 2' => array (
                                    'name' 	=> 	'Red Title',
                                    'element' 	=> 	'h2',
                                    'styles' => array(
                                            'color' 		=> 	'Red',
                                            'font-weight' 		=> 	'bold',
                                            'text-decoration'	=> 	'underline'
                                    )
                            )				
                    )
            );
            $this->load->view('admin/header',$data);
        }

        public function index(){
            if($this->input->post('year')){                
                $year = $this->input->post('year');
                $check = $this->MediakitModel->checkYear($year);
                if($check == 0){
                    echo 'ok';
                    exit;
                }
                else {
                    echo 'error';
                    exit;
                }                
            }
            $id = 1;            
            $data['about'] = $this->AdvertisingModel->getAdvertising($id);
            $data['last_date']=$this->MediakitModel->getLastDate();
            $data['mediakits']=$this->MediakitModel->getAll();
            $this->load->library('ckeditor');
            $this->load->library('ckfinder');
            $this->ckeditor->basePath = base_url().'/public/plugins/ckeditor/';
            $this->ckeditor->config['language'] = 'ru';
            $this->ckeditor->config['width'] = '100%';
            $this->ckeditor->config['height'] = '300px';             
            $this->ckfinder->SetupCKEditor($this->ckeditor,'/public/plugins/ckfinder/');
            $this->form_validation->set_rules('description', 'Page Description', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/advertising', $data);
            $this->load->view('admin/footer');
            }
            if($this->input->post('upload_mediakit')){  

                if($_FILES['file']['name'] == '' || $_FILES['cover']['name'] == '' || $this->input->post('year') == ''){                    
                    $this->session->set_flashdata('magazine-error','<div class="alert alert-danger text-center">Заполните все поля</div>');
                    header('Location: '.$_SERVER['REQUEST_URI']);
                    exit;
                }                
                $year = $this->input->post('year');
                
                $mediakit_data = [                                            
                    'year' => $year,

                ];
                $res = $this->MediakitModel->addMediakit($mediakit_data);
                                
                if($res){
                    $path = 'ci/userfiles/mediakit/'.$res;
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    $config['upload_path'] = $path;
                    $config['encrypt_name'] = TRUE;
                    $config['allowed_types'] = 'jpg|png|jpeg|gif';                
                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('cover'))
                    {
                        $error = array('error' => $this->upload->display_errors());                                                
                    }
                    else {
                        $data = array('upload_data' => $this->upload->data());                            
                        $image = $data['upload_data']['file_name'];
                        $update_data = ['image' => $image];
                        $result = $this->MediakitModel->editMediakit($res, $update_data);                        
                    }
                    
                    $config['upload_path'] = $path;
                    $config['encrypt_name'] = TRUE;
                    $config['allowed_types'] = 'pdf';     
                    $config['max_size'] = 0;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ( ! $this->upload->do_upload('file'))
                    {
                        $error = array('error' => $this->upload->display_errors());                          
                    }
                    else {
                        $data = array('upload_data' => $this->upload->data());                            
                        $file = $data['upload_data']['file_name'];
                        $update_data = ['file_name' => $file];
                        $result = $this->MediakitModel->editMediakit($res, $update_data);                        
                    }
                    $this->session->set_flashdata('magazine-added','<div class="alert alert-success text-center">Медиакит добавлен</div>');
                    header('Location: '.$_SERVER['REQUEST_URI']);
                    

                }                                
            }
            
            if($this->input->post('addAdvertising')){                
                $ru_text = $this->input->post('ru_text');
                $en_text = $this->input->post('en_text');
                $about_data = [
                    'ru_text' => $ru_text,
                    'en_text' => $en_text
                ];
                $res = $this->AdvertisingModel->updateAdvertising($id,$about_data);
                if($res){
                    $this->session->set_flashdata('magazine-added','<div class="alert alert-success text-center">Информация обновлена</div>');
                    header('Location: /admin/advertising');
                }
                
            }
        }
            

}
