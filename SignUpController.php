<?php

class SignUpController extends MY_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('UsersModel');
        $this->load->helper('form');

    }
    public function index(){
        if($this->input->post('signup')){
            $f_name = $this->input->post('f_name');
            $l_name = $this->input->post('l_name');
            $username = $this->input->post('username');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $address = $this->input->post('address');
            $password = $this->input->post('password');
            $submitted = $this->input->post();
            if($f_name == '' || $l_name == '' || $username == '' || $email == '' || $password == '' || $phone == '' || $address == ''){
                
                $this->session->set_flashdata('error','<div class="alert alert-danger text-center">Fill all fields</div>');
                $this->session->set_flashdata('submitted',$submitted); 

                redirect(base_url('/signup'));
            }
            $checkEmail = $this->UsersModel->checkEmail($email);
            
            if($checkEmail > 0){
                $this->session->set_flashdata('error','<div class="alert alert-danger text-center">The E-Mail address already registered.</div>');
                $this->session->set_flashdata('submitted',$submitted); 

                redirect(base_url('/signup'));
                exit;
            }            
            $checkUsername = $this->UsersModel->checkUsername($username);
            
            if($checkUsername > 0){
                $this->session->set_flashdata('error','<div class="alert alert-danger text-center">The username already registered.</div>');
                $this->session->set_flashdata('submitted',$submitted); 

                redirect(base_url('/signup'));
                exit;
            }

            $user_data = [
                'first_name' => $f_name,
                'last_name' => $l_name,
                'username' => $username,
                'email' => $email,
                'phone' => $phone,
                'address' => $address,
                'password' => md5($password)
            ];
            $result = $this->UsersModel->addUser($user_data);
            if($result){
                $this->session->set_flashdata('success','<div class="alert alert-success text-center">You are successfully registered</div>');
                redirect('signin');
            }
        }
        $this->layout->viewHome('signup');
    }
    
    public function signout(){
        $this->session->sess_destroy();
        redirect('/');
    }
}