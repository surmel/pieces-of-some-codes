<?php
    class ModelsModel extends CI_Model{
        
        public function getAll() {
            return $this->db->select('m.id as model_id, m.model, b.id as brand_id, b.brand')
							->join('brands b', 'm.brand_id = b.id')	
							->order_by('m.id', 'desc')
							->get('models m')
							->result_array();
        }
        public function addModel($model_data){            
            return $this->db->insert('models',$model_data);             
        }
        
        public function deleteBrandModels($id){
            return $this->db->where('brand_id', $id)->delete('models');
        }        
        public function getById($id){
            return $this->db->select('m.id as model_id, m.model, b.id as brand_id, b.brand')->join('brands b', 'm.brand_id = b.id')->where('m.id', $id)->get('models m')->row_array();
        }
        public function getBrandModels($id){
            return $this->db->select('*')->where('brand_id', $id)->get('models')->result_array();
        }

        public function deleteModel($id){
            return $this->db->where('id', $id)->delete('models');
        }

        public function editModel($id, $data){            
            return $this->db->where('id', $id)->update('models', $data);
        }
        
    }

