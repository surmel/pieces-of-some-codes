<div class="container">
    <div class="row">
        <div class="col-md-9 col-sm-12 col-xs-12 m-t20">
            <div class="page_block">
                <p class="container-title"><?=$this->lang->line('last_news');?></p>
                <?php foreach($news as $new): ?>                
                <div class="news_container">
                    <div class="col-md-4">
                        <a href="/news/<?=$new['id']?>"><img src="<?=base_url('ci/userfiles/news/'.$new['category'].'/'.$new['id'].'/'.$new['primary_image'])?>" class="thumbnail" style="max-height: 180px;max-width: 100%;display: block;margin: 0px auto 10px auto;"></a>
                    </div>
                    <div class="col-md-8">
                        <a href="/news/<?=$new['id']?>"><p class="news_title"><?=$new[$lang.'_title']?></p></a>
                        <?=$new['date']?></p>
                        <?=$new['category']?></p>
                        <p class="news_text"><?=$new[$lang.'_description']?></p>
                    </div>                    
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 m-t20">            
            <div class="page_block" style="margin-bottom: 10px;">
                <p class="container-title"><?=$this->lang->line('last_number');?><a href="<?=base_url('archive')?>"><input type="button" class="btn btn-primary archive_button" value="<?=$this->lang->line('archive');?>"></a></p> 
                <?php if(!empty($magazine)): ?>
                <a href="<?= base_url('ci/userfiles/magazine/'.$magazine['id'].'/'.$magazine['file_name'])?>" target="blank"><img src="<?= base_url('ci/userfiles/magazine/'.$magazine['id'].'/'.$magazine['image'])?>" class="thumbnail cover_image"></a>
                <p class="magazine_name"><a href="<?= base_url('ci/userfiles/magazine/'.$magazine['id'].'/'.$magazine['file_name'])?>" target="blank">№<?=$magazine['number']?>, <?=$magazine[$lang.'_month']?>, <?=$magazine['year']?></a></p>
                <?php else:?>
                
                <?php endif;?>
            </div>
            
            <div class="page_block">
                <p class="container-title"><?=$this->lang->line('event_calendar');?></p>
                <div id="eventCalendarLocaleFile"></div>				
            </div>            
        </div>
    </div>
</div>