<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('path');
        $this->load->helper('security');
        $this->load->model('Admin');
        $this->load->library(array('session', 'form_validation', 'email'));
    }
    public function index(){
        if($this->input->post('submit')){                
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));                
            $result = $this->Admin->login($username, $password);
            if($result == false){
                $this->session->set_flashdata('error','<div class="alert alert-danger text-center">Wrong username or password</div>');
                header('Location: '.$_SERVER['REQUEST_URI']);
                exit;
            }
            else {
                $this->session->set_userdata(array('username' => $username));
                header('Location: /admin/dashboard');
                exit;
            }                
        }
        $this->load->view('admin/login');
    }

    public function passwordReset(){
        if($this->input->post('submit')){
            $email = $this->input->post('email');
            $id = $this->Admin->checkEmail($email);

            if($id == false){
                $this->session->set_flashdata('error','<div class="alert alert-danger text-center">Wrong E-Mail Address</div>');
                header('Location: '.$_SERVER['REQUEST_URI']);
            }
            else {
                //configure email settings
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = 'ssl://smtp.gmail.com';
                $config['smtp_port'] = '465';
                $config['smtp_user'] = '';
                $config['smtp_pass'] = '';
                $config['mailtype'] = 'html';
                $config['charset'] = 'utf-8';
                $config['wordwrap'] = TRUE;
                $config['newline'] = "\r\n"; //use double quotes
                $this->email->initialize($config);   

                $token =  md5(rand(5,20));
                $date = date('Y-m-d H:i:s');
                $data = array (
                        'reset_token' => $token,
                        'token_date' => $date
                );
                $this->Admin->updateToken($id, $data);

                $link = base_url('admin/newPassword/'.$id.'/'.$token);

                $message = '<p>Click <a href="'.$link.'" >here</a>, to recover Your password.</p>'; 


                //send mail
                $this->email->from('test@test.am');
                $this->email->to($email);
                $this->email->subject('Message from web site');
                $this->email->message($message);
                if ($this->email->send())
                {
                    // mail sent
                    $this->session->set_flashdata('error','<div class="alert alert-success text-center">Password recovery link sent to Your E-Mail!</div>');
                    header('Location: /admin');
                }
                else {
                    var_dump('something is wrong');
                }
            }
        }
        $this->load->view('admin/reset_password');
    }

    public function newPassword($id, $token){
        $token_data = $this->Admin->getToken($id);

        $date = date('Y-m-d H:i:s');

        if($token == $token_data['reset_token'] && strtotime($token_data['token_date']) + 86400 > strtotime($date)){
            if($this->input->post('submit')){
                
                $newpass = $this->input->post('newpassword');                
                $confirmnewpass = $this->input->post('confirmpassword');
                if(!preg_match('/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/', $newpass)){                 
                    $this->session->set_flashdata('passerror','<div class="alert alert-danger text-center">Пароль должен состоять из комбинации латинких букв и цифр</div>');
                    header('Location: '.$_SERVER['REQUEST_URI']);
                }                
                if($newpass != $confirmnewpass){
                    $this->session->set_flashdata('passerror','<div class="alert alert-danger text-center">Пароли не совпадают</div>');
                    header('Location: '.$_SERVER['REQUEST_URI']);
                }
                else {
                    $newpass = md5($newpass);

                    $passarray = array (
                            'password' => $newpass,
                            'reset_token' => $token,
                            'token_date' => $date,
                            'updated_at' => $date,
                    );

                    $result = $this->Admin->resetPass($id, $passarray);
                    if($result == true){
                        $this->session->set_flashdata('error','<div class="alert alert-success text-center">Пароль успешно изменен</div>');
                        header('Location: /admin');
                    }
                }
            }
            $this->load->view('admin/newpassword');
        }
        else {
            $this->session->set_flashdata('error','<div class="alert alert-danger text-center">Срок действия ссылки истек</div>');
            header('Location: /admin');
        }
    }    
    
    public function logout(){
        $this->session->unset_userdata('username');
        header('Location: /admin');
    }
}
