<?php
    class EventsModel extends CI_Model{
        
        public function getAll() {
            return $this->db->select('*')->order_by('id', 'desc')->get('events')->result_array();
        }
        public function getFileName($id){
            return $this->db->select('image')->where('id', $id)->get('events')->row_array();
            
        }
        public function getById($id){
            return $this->db->select('*')->where('id', $id)->get('events')->row_array();
        }

        public function deleteEvent($id){
            return $this->db->where('id', $id)->delete('events');
        }
        public function addEvent($event_data){            
            $this->db->insert('events',$event_data); 
            return $this->db->insert_id();
            
        }
        public function editEvent($id, $data){            
            return $this->db->where('id', $id)->update('events', $data);
        }
        
    }

