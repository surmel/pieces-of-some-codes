<?php

class PaymentConfirmationController extends MY_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('PaymentConfirmationModel');
        $this->load->model('OrdersModel');
        $this->load->helper('form');
    }
    public function index(){
        if($this->input->post('send_confirm')){
            $submitted = $this->input->post();
            $order_id = $this->input->post('order_id');
            $bank_name = $this->input->post('bank_name');
            $account_number = $this->input->post('account_number');
            $account_name = $this->input->post('account_name');
            $payment_total = $this->input->post('payment_total');
            if($order_id == '' || $bank_name == '' || $account_number == '' || $account_name == '' || $payment_total == ''){
                $this->session->set_flashdata('error','<div class="alert alert-danger text-center">Fill all fields</div>');
                $this->session->set_flashdata('submitted',$submitted); 

                redirect(base_url('/paymentConfirmation'));
            }
            $order_id = str_replace(' ', '', $order_id);
            $checkOrderId = $this->OrdersModel->checkOrderId($order_id);

            if($checkOrderId === 0){
                $this->session->set_flashdata('error','<div class="alert alert-danger text-center">Wrong Order ID</div>');
                $this->session->set_flashdata('submitted',$submitted); 

                redirect(base_url('/paymentConfirmation'));
            }
            else {
                $payment_info = [
                    'order_id' => $order_id,
                    'bank_name' => $bank_name,
                    'account_number' => $account_number,
                    'account_name' => $account_name,
                    'payment_total' => $payment_total
                ];
                
                $this->PaymentConfirmationModel->addConfirmation($payment_info);
                $updateConfirmation = ['confirmation' => 1];
                $res = $this->OrdersModel->updateConfirmation($order_id, $updateConfirmation);
                if($res){
                    $this->session->set_flashdata('error','<div class="alert alert-success text-center">Thank You. Your confirmation was received.</div>'); 

                    redirect(base_url('/'));
                }
            }
        }
        $this->layout->viewHome('payment_information');
    }
}